#include <iostream>
#include <exception>
#include <vector>
#include <thread>
#include <chrono>
#include <atomic>

#include "CuCompressor.h"
#include "CuDecompressor.h"

using std::cout;
using std::exception;
using std::vector;
using std::thread;
using namespace std::chrono_literals;

enum class State 
{
    Hello,
    Compressing,
    CompressingDone,
    Decompressing,
    DecompressingDone,
    Done
};

void print55(const vector<int>& v) {
    for (size_t i = 0; i < 5; i++) {
        cout << v[i] << ", ";
    }
    cout << "..., ";
    for (size_t i = v.size()-5; i < v.size()-1; i++) {
        cout << v[i] << ", ";
    }
    cout << v.back();
    cout << "\n";
}

int main() try {
    //num of elements; 1000000 is too fast
    const size_t N = 100000000UL;
    // input array
    vector<int> input;
    // compressed input array
    vector<uint8_t> compressed;
    // decompressed compressed input array, shoud be equal to input
    vector<int> decompressed;
    // to track current state of things
    std::atomic<State> state = State::Hello;

    // to calc time for comp/decomp
    std::atomic<typeof(std::chrono::high_resolution_clock::now())> timeStart;
    std::atomic<typeof(std::chrono::high_resolution_clock::now())> timeEnd;

    // simple finished for compressor
    const auto CompressFinished = [&](vector<uint8_t>&& r){
        compressed = std::move(r);
        timeEnd = std::chrono::high_resolution_clock::now();
        state = State::CompressingDone;
    };
    
    // simple finished for decompressor
    const auto DecompressFinished = [&](vector<int>&& r){
        decompressed = std::move(r);
        timeEnd = std::chrono::high_resolution_clock::now();
        state = State::DecompressingDone;
    };

    const auto Compress = [&]{
        state = State::Compressing;
        timeStart = std::chrono::high_resolution_clock::now();
        cu::CuCompressor comp;
        comp.onCompressFinished = CompressFinished;
        comp.setData(input.data(), input.size());
        comp.doCompress();
    };

    const auto Decompress = [&]{
        state = State::Decompressing;
        timeStart = std::chrono::high_resolution_clock::now();
        cu::CuDecompressor decomp;
        decomp.onDecompressFinished = DecompressFinished;
        decomp.setData(compressed.data(), compressed.size());
        decomp.doDecompress();
    };

    // fill the input array!
    std::srand (time(NULL));
    input.resize(N);
    for (size_t i = 0; i < N; i++) {
        input[i] = 1 + std::rand()/((RAND_MAX + 1u)/1000);
    }

    thread trdCompress;
    thread trdDecompress;

    while(state != State::Done) {
        switch (state) {
            case State::Hello: {
                cout << "Hello cucompress!\n";
                cout << "Now we will compress and decompress stuff.\n";
                cout << "N = " << N << "\n\n";
                cout << "Starting compression.";
                trdCompress = thread{Compress};
                break;
            }
            case State::Compressing: {
                cout << ".";
                break;
            }
            case State::CompressingDone: {
                auto dt = (timeEnd.load() - timeStart.load()).count() / 1000000;
                cout << "\nCompressing done in " << dt << "ms\n";
                auto rate = compressed.size() * 100 / (input.size() * sizeof(int));
                cout << "Compession rate: " << rate << "%\n\n";
                cout << "Starting decompression.";
                state = State::Decompressing;
                trdDecompress = thread{Decompress};
                timeStart = std::chrono::high_resolution_clock::now();
                break;
            }
            case State::Decompressing: {
                cout << ".";
                break;                
            }
            case State::DecompressingDone: {
                auto dt = (timeEnd.load() - timeStart.load()).count() / 1000000;
                cout << "\nDecompression done in " << dt << "ms\n\n";
                cout << "Input data :\n\t";
                print55(input);
                cout << "Output data :\n\t";
                print55(decompressed);
                state = State::Done;
                break;
            }
        }
        cout.flush();
        std::this_thread::sleep_for(30ms);
    }
    trdCompress.join();
    trdDecompress.join();

    return 0;
} catch(exception& e) {
    cout << "ERROR: " << e.what();
}