# Cucompress

A small project to play around with [nvcomp](https://github.com/NVIDIA/nvcomp);
Incorporates some simple wrapper to compress/uncompress the block of integers. The choosen algorithm seems to be quite good for this application.

Limitations: 
* Maximum data size would be 2^31-1 due to nvcomp's limitations.

## Build

The build envinronment is set up using the docker image in .devcontainer directory.
The simplest way to build would be to use Visual Studio Code with Docker Tools extension.

Other requirements:
1. Docker 3.19
2. NVidia Container Toolkit (https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker)
3. Github Cli with set up credentials using the SSH method (to pull things in the container). (https://github.com/cli/cli#installation)
4. Pascal (or higher) GPU
5. NVidia driver v.470 or newer

To build without the docker image:

6. Cuda 11.4
7. Install NVComp (https://github.com/NVIDIA/nvcomp)
8. Install Catch2 (for building the tests)

## Run

You could choose to run tests or the main application, the second one is just a bit more entertaining with a state-machine-ish approach and multithreading.

## Things to improve

* Create a common base class for CuCompressor and CuDecompressor;
* Implement some stream-based compression using low-level nvcomp API;
* Implement compression of bigger amounts of data by splitting the data into chunks;
* ...

## Example output of Cucompress

Done with Ubuntu 18.04.6 LTS / Intel® Core™ i7-8750H CPU / Quadro P600 / Driver Version: 470.63.01 / CUDA Version: 11.4

```
Hello cucompress!
Now we will compress and decompress stuff.
N = 100000000

Starting compression..........
Compressing done in 299ms
Compession rate: 31%

Starting decompression...........
Decompression done in 302ms

Input data :
        841, 395, 784, 799, 912, ..., 599, 774, 253, 191, 989
Output data :
        841, 395, 784, 799, 912, ..., 599, 774, 253, 191, 989
```


