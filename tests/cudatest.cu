#include "cudatest.h"
#include <exception>
#include <iostream>

__global__ void add(int *a, int *b, int *c) {
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    c[idx] = a[idx] + b[idx];
}


#define handleCudaError(x) \
    do { \
        auto r = x; \
        if (r != cudaSuccess) { \
            std::cout << #x; \
            clearData(); \
            return r; } \
    } while(false)

extern "C" int cudatest() {
    const size_t size = 2 << 10;
    const size_t tpb = 2 << 4;

    int * carray1 = 0;
    int * carray2 = 0;
    int * carray3 = 0;
    int * array1 = 0;
    int * array2 = 0;
    int * array3 = 0;

    const auto clearData = [&]{
        if (array1) free(array1);
        if (array2) free(array2);
        if (array3) free(array3);
        if (carray1) cudaFree(carray1);
        if (carray2) cudaFree(carray2);
        if (carray3) cudaFree(carray3);
    };

    handleCudaError(cudaMalloc((void**)&carray1, size * sizeof(int)));
    handleCudaError(cudaMalloc((void**)&carray2, size * sizeof(int)));
    handleCudaError(cudaMalloc((void**)&carray3, size * sizeof(int)));

    array1 = (int*)malloc(size * sizeof(int));
    array2 = (int*)malloc(size * sizeof(int));
    array3 = (int*)malloc(size * sizeof(int));
    for (size_t i = 0; i < size; i++) {
        array1[i] = i;
        array2[i] = -2 * (int)i;
    }

    handleCudaError(cudaMemcpy((void*)carray1, (void*)array1, size * sizeof(int), cudaMemcpyHostToDevice));
    handleCudaError(cudaMemcpy((void*)carray2, (void*)array2, size * sizeof(int), cudaMemcpyHostToDevice));

    add<<<size / tpb, tpb>>>(carray1, carray2, carray3);
    handleCudaError(cudaGetLastError());

    handleCudaError(cudaMemcpy((void*)array3, (void*)carray3, size * sizeof(int), cudaMemcpyDeviceToHost));

    const int result = [&]{
        for (int i = 0; i < size; i++) {
            if (array3[i] != -i) return array3[i];
        }
        return 0;
    }();

    clearData();

    return result;
}



