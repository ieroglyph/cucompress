#include <catch2/catch_test_macros.hpp>

#include <exception>
#include <memory>
#include <random>
#include <vector>
#include <iostream>

#include <nvcomp/cascaded.hpp>
#include <nvcomp.hpp>

#include "cudatest.h"
#include "CuCompressor.h"
#include "CuDecompressor.h"

using std::exception;
using std::unique_ptr;
using std::vector;
using namespace nvcomp;

TEST_CASE("Tests are working", "[test]") {
    REQUIRE(2 * 2 == 4);
}

TEST_CASE("Cuda is working", "[cuda]") {
    SECTION("Cuda support is OK") {
        int deviceCount = 0;
        cudaGetDeviceCount(&deviceCount);
        REQUIRE(deviceCount > 0);
    }
    SECTION("Simple addition is possible") {
        REQUIRE(cudatest() == 0);
    }
}

TEST_CASE("nvcomp is working", "[nvcomp]") {
    try {
        // additional helper stuff
        const auto handleCudaError = []{
            if (auto e = cudaGetLastError(); e != cudaSuccess)
                throw new std::runtime_error(cudaGetErrorString(e));
        };


        SECTION("Cuda support is OK") {
            int deviceCount = 0;
            cudaGetDeviceCount(&deviceCount);
            REQUIRE(deviceCount > 0);
        }

        const size_t N = 2 << 12; 

        vector<int> input;
        input.resize(N);
        for (size_t i = 0; i < N; i++) {
            input[i] = 1 + std::rand()/((RAND_MAX + 1u)/1000);
        }

        vector<int> output;
        output.resize(N);
        
        cudaStream_t stream;
        cudaStreamCreate(&stream);
        handleCudaError();
        
        const size_t uncompressed_bytes = N * sizeof(int);
        size_t temp_bytes = 0;
        size_t output_bytes = 0;
        size_t compressed_bytes = 0;
        size_t decompressed_bytes = 0;
        size_t * d_compressed_bytes = nullptr;
        void * uncompressed_data = nullptr;
        void * temp_space = nullptr;
        void * compressed_space = nullptr;
        int* decompressed_space = nullptr;

        CascadedCompressor compressor(TypeOf<int>());

        compressor.configure(uncompressed_bytes, &temp_bytes, &output_bytes);
        REQUIRE(temp_bytes > 0);
        REQUIRE(output_bytes > 0);

        cudaMalloc(&uncompressed_data, uncompressed_bytes);
        handleCudaError();

        cudaMemcpy(uncompressed_data, input.data(), uncompressed_bytes,cudaMemcpyHostToDevice);
        handleCudaError();

        cudaMalloc(&temp_space, temp_bytes);
        handleCudaError();

        cudaMalloc(&compressed_space, output_bytes);
        handleCudaError();

        cudaMallocHost((void**)&d_compressed_bytes, sizeof(*d_compressed_bytes));
        handleCudaError();

        compressor.compress_async(uncompressed_data, uncompressed_bytes,
            temp_space, temp_bytes, compressed_space, d_compressed_bytes, stream);

        cudaStreamSynchronize(stream);
        handleCudaError();

        REQUIRE(d_compressed_bytes != nullptr);
        compressed_bytes = *d_compressed_bytes;
        REQUIRE(compressed_bytes > 0);
        REQUIRE(compressed_bytes < uncompressed_bytes);

        CascadedDecompressor decompressor;
        decompressor.configure(
            compressed_space, compressed_bytes, &temp_bytes, &decompressed_bytes, stream);

        cudaMalloc(&temp_space, temp_bytes);
        handleCudaError();

        cudaMalloc(&decompressed_space, decompressed_bytes);
        handleCudaError();

        decompressor.decompress_async(
            compressed_space,
            compressed_bytes,
            temp_space,
            temp_bytes,
            decompressed_space,
            decompressed_bytes,
            stream);

        cudaStreamSynchronize(stream);
        handleCudaError();

        REQUIRE(decompressed_bytes == uncompressed_bytes);
        
        cudaMemcpy(output.data(), decompressed_space, decompressed_bytes, cudaMemcpyDeviceToHost);

        REQUIRE(input == output);

        cudaFree(uncompressed_data);
        cudaFree(temp_space);
        cudaFree(compressed_space);
        cudaFree(temp_space);
        cudaFree(decompressed_space);
        cudaFreeHost(d_compressed_bytes);

    } catch(exception& e) {
        FAIL(e.what());
    } catch (...) {
        FAIL("Unknown exception");
    }
}

TEST_CASE("CuCompress", "[cu]")
{
    try {
        vector<int> input;
        const size_t N = 1000000;
        input.resize(N);
        for (int i = 0; i < N; i++) {
            input[i] = 1 + std::rand()/((RAND_MAX + 1u)/1000);
        }
        
        cu::CuCompressor comp;
        comp.setData(input.data(), input.size());

        int finishTriggeredC = 0;
        vector<uint8_t> compressed;

        const auto onFinishC = [&](vector<uint8_t>&& r) { 
            finishTriggeredC++; compressed = std::move(r); 
        };

        comp.onCompressFinished = onFinishC;
        comp.doCompress();

        REQUIRE(finishTriggeredC == 1);
        REQUIRE(compressed.size() > 0);

        cu::CuDecompressor decomp;
        decomp.setData(compressed.data(), compressed.size());

        int finishTriggeredD = 0;
        vector<int> decompressed;
        const auto onFinishD = [&](vector<int>&& r) { 
            finishTriggeredD++; decompressed = std::move(r); 
        };

        decomp.onDecompressFinished = onFinishD;
        decomp.doDecompress();
        
        REQUIRE(finishTriggeredD == 1);
        REQUIRE(decompressed.size() > 0);

        REQUIRE(input.size() == decompressed.size());
        REQUIRE(input == decompressed);
    } catch(exception* e) {
        FAIL(e->what());
    } catch (...) {
        FAIL("Unknown exception");
    }
}