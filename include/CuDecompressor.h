#pragma once
#include <functional>
#include <memory>
#include <vector>

using std::function;
using std::unique_ptr;
using std::vector;

namespace cu {
  /**
   * @brief An object responsible for handling of the decompression stuff
   * 
   */   
class CuDecompressor
{
public:
    /**
     * @brief Construct a new Cu Decompressor object
     * 
     * Throws std::runtime_error if no CUDA devices could be found
     */
    CuDecompressor();
    ~CuDecompressor();

    // disable copying
    CuDecompressor(const CuDecompressor&) = delete;
    CuDecompressor(const CuDecompressor&&) = delete;
    CuDecompressor& operator=(const CuDecompressor&) = delete;

    /**
     * @brief Set the Data object
     * 
     * The data may not become copied to the CuDecompressor's internal memory at once,
     * so the user should guarantee its long-term accessibility.
     * 
     * @note All decompressed data from the prevoius operations (if any) will be erased! 
     * 
     * @param data Pointer to the data
     * @param dataSize Size of the data, in elements
     */
    void setData(uint8_t* data, const size_t dataSize);

    /**
     * @brief Do the job
     */
    void doDecompress();

    /**
     * @brief Callback to call when the decompression is finished
     */
    function<void(vector<int>&&)> onDecompressFinished;

private:
    struct PData;
    unique_ptr<PData> pData;
};
}