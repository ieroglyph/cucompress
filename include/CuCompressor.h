#pragma once
#include <functional>
#include <memory>
#include <vector>

using std::function;
using std::unique_ptr;
using std::vector;

namespace cu {
  /**
   * @brief An object responsible for handling of the compression stuff
   * 
   * No configurable options, but we could always add them if needed.
   * By default, the most optimal parameters should be set automatically
   */   
class CuCompressor
{
public:
    /**
     * @brief Construct a new Cu Compressor object
     * 
     * Throws std::runtime_error if no CUDA devices could be found
     */
    CuCompressor();
    ~CuCompressor();

    // disable copying
    CuCompressor(const CuCompressor&) = delete;
    CuCompressor(const CuCompressor&&) = delete;
    CuCompressor& operator=(const CuCompressor&) = delete;

    /**
     * @brief Set the Data object
     * 
     * The data may not become copied to the CuCompressor's internal memory at once,
     * so the user should guarantee its long-term accessibility.
     * 
     * @note All compressed data from the prevoius operations (if any) will be erased! 
     * 
     * @param data Pointer to the data
     * @param dataSize Size of the data, in elements
     */
    void setData(int* data, const size_t dataSize);

    /**
     * @brief Do the job
     */
    void doCompress();

    /**
     * @brief Callback to call when the compression is finished
     */
    function<void(vector<uint8_t>&&)> onCompressFinished;

private:
    struct PData;
    unique_ptr<PData> pData;
};
}