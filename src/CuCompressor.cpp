#include "CuCompressor.h"

#include <vector> 
#include <iostream>

#include <cuda.h>
#include <cuda_runtime.h>
#include <nvcomp/cascaded.hpp>
#include <nvcomp.hpp>

using namespace cu;
using namespace nvcomp;
using std::vector;
using std::cout;

const auto handleCudaError = [](cudaError e){
    if (e != cudaSuccess)
        throw new std::runtime_error(cudaGetErrorString(e));
};

struct CuCompressor::PData
{
    int* data = nullptr;
    size_t dataSize = 0;
};

CuCompressor::CuCompressor()
{
    pData = std::unique_ptr<PData>(new PData());
    int deviceCount = 0;
    cudaGetDeviceCount(&deviceCount);
    if (deviceCount < 1)
        throw std::runtime_error("No CUDA devices found!");
}

CuCompressor::~CuCompressor()
{
    //nothing special, really
}

void CuCompressor::doCompress() 
{
    cudaStream_t stream;
    handleCudaError(cudaStreamCreate(&stream));

    const size_t uncompressedElements = pData->dataSize;
    const size_t uncompressedBytes = uncompressedElements * sizeof(int);
    size_t tempBytes = 0;
    size_t outputBytes = 0;
    size_t compressedBytes = 0;
    size_t * dCompressedBytes = nullptr;
    void * uncompressedData = nullptr;
    void * tempSpace = nullptr;
    void * compressedSpace = nullptr;   

    // the optimal parameters should be set here, hopefuly,
    // according to the docs
    CascadedCompressor compressor(TypeOf<int>());
    compressor.configure(uncompressedBytes, &tempBytes, &outputBytes);

    // first we malloc all the needed stuff to reuse that in every iteration
    handleCudaError(cudaMalloc(&uncompressedData, uncompressedBytes));
    handleCudaError(cudaMalloc(&tempSpace, tempBytes));
    handleCudaError(cudaMalloc(&compressedSpace, outputBytes));
    handleCudaError(cudaMallocHost((void**)&dCompressedBytes, sizeof(*dCompressedBytes)));

    handleCudaError(cudaMemcpy(uncompressedData, pData->data, uncompressedBytes, cudaMemcpyHostToDevice));

    compressor.compress_async(uncompressedData, uncompressedBytes,
        tempSpace, tempBytes, compressedSpace, dCompressedBytes, stream);

    handleCudaError(cudaStreamSynchronize(stream));

    compressedBytes = *dCompressedBytes;

    vector<uint8_t> outputdata;
    outputdata.resize(compressedBytes);

    handleCudaError(cudaMemcpy(outputdata.data(), compressedSpace, compressedBytes, cudaMemcpyDeviceToHost));

    cudaFree(uncompressedData);
    cudaFree(tempSpace);
    cudaFree(compressedSpace);
    cudaFreeHost(dCompressedBytes);

    onCompressFinished(std::move(outputdata));
}

void CuCompressor::setData(int* data, const size_t dataSize)
{
    pData->data = data;
    pData->dataSize = dataSize;
}