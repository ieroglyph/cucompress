#include "CuDecompressor.h"

#include <vector> 
#include <iostream>

#include <cuda.h>
#include <cuda_runtime.h>
#include <nvcomp/cascaded.hpp>
#include <nvcomp.hpp>

using namespace cu;
using namespace nvcomp;
using std::vector;
using std::cout;

const auto handleCudaError = [](cudaError e){
    if (e != cudaSuccess)
        throw new std::runtime_error(cudaGetErrorString(e));
};

struct CuDecompressor::PData
{
    uint8_t* data = nullptr;
    size_t dataSize = 0;
};

CuDecompressor::CuDecompressor()
{
    pData = std::unique_ptr<PData>(new PData());
    int deviceCount = 0;
    cudaGetDeviceCount(&deviceCount);
    if (deviceCount < 1)
        throw std::runtime_error("No CUDA devices found!");
}

CuDecompressor::~CuDecompressor()
{
    //nothing special, really
}

void CuDecompressor::doDecompress() 
{
    cudaStream_t stream;
    handleCudaError(cudaStreamCreate(&stream));

    const size_t compressedElements = pData->dataSize;
    const size_t compressedBytes = compressedElements * sizeof(uint8_t);
    size_t tempBytes = 0;
    size_t outputBytes = 0;
    void * compressedData = nullptr;
    void * tempSpace = nullptr;
    void * decompressedSpace = nullptr;   

    handleCudaError(cudaMalloc(&compressedData, compressedBytes));
    handleCudaError(cudaMemcpy(compressedData, pData->data, compressedBytes, cudaMemcpyHostToDevice));

    // the optimal parameters should be set here, hopefuly,
    // according to the docs
    CascadedDecompressor decompressor;
    decompressor.configure(
            compressedData, compressedBytes, &tempBytes, &outputBytes, stream);

    handleCudaError(cudaMalloc(&tempSpace, tempBytes));
    handleCudaError(cudaMalloc(&decompressedSpace, outputBytes));

    decompressor.decompress_async(
            compressedData, compressedBytes,
            tempSpace, tempBytes,
            decompressedSpace, outputBytes,
            stream);

    handleCudaError(cudaStreamSynchronize(stream));

    vector<int> outputdata;
    outputdata.resize(outputBytes / sizeof(int));

    handleCudaError(cudaMemcpy(outputdata.data(), decompressedSpace, outputBytes, cudaMemcpyDeviceToHost));

    cudaFree(compressedData);
    cudaFree(tempSpace);
    cudaFree(decompressedSpace);

    onDecompressFinished(std::move(outputdata));
}

void CuDecompressor::setData(uint8_t* data, const size_t dataSize)
{
    pData->data = data;
    pData->dataSize = dataSize;
}