FROM nvidia/cuda:11.4.2-devel-ubuntu20.04

LABEL Name=cucompare Version=0.0.1

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/London

RUN apt-get -y update && apt-get install -y

RUN apt-get -y install --no-install-recommends\
    apt-transport-https ca-certificates gnupg \
                         software-properties-common wget \
        gcc-10 \
        g++-10 \
        build-essential \
        linux-headers-$(uname -r) \
	    git \
        wget

RUN wget -qO - https://apt.kitware.com/keys/kitware-archive-latest.asc | apt-key add - && \
    apt-add-repository 'deb https://apt.kitware.com/ubuntu/ focal main' && \
    apt-get -y update && \
    apt-get -y install cmake 

ADD . /cucompare/
WORKDIR /cucompare

RUN cd /cucompare && \
    git clone https://github.com/NVIDIA/nvcomp.git && \
    cd ./nvcomp && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make -j && \
    make install

RUN cd /cucompare && \
    git clone https://github.com/catchorg/Catch2.git && \
    cd Catch2 && \
    cmake -Bbuild -H. -DBUILD_TESTING=OFF && \
    cmake --build build/ --target install
