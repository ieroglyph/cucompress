cmake_minimum_required(VERSION 3.18.0)
project(Cucompress VERSION 0.1.0 LANGUAGES CXX CUDA)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")

file(GLOB_RECURSE SOURCES "src/*")
file(GLOB_RECURSE INCLUDES "include/*")

find_package(CUDA REQUIRED)
find_package(nvcomp REQUIRED)

add_executable(${PROJECT_NAME} "main.cpp")
target_sources(${PROJECT_NAME} PRIVATE ${SOURCES} ${INCLUDES})
target_link_libraries(${PROJECT_NAME} nvcomp ${CUDA_LIBRARIES})

include_directories("include" "${CUDA_INCLUDE_DIRS}")

set(TESTS_ENABLED true)

if(TESTS_ENABLED)
    add_subdirectory(tests)
endif()